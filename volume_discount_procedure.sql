DELIMITER $$

CREATE PROCEDURE getProductVolumeDiscounts(v_customer_external_id VARCHAR(255), v_sku VARCHAR(255), v_pricelistid VARCHAR(100), v_itemgroupid VARCHAR(100))
BEGIN
    SET @vCustomerExternalId = v_customer_external_id;
    SET @vSku = v_sku;
    SET @vPriceListId = v_pricelistid;
    SET @vItemGroupId = v_itemgroupid;
    SET @PriceFound = FALSE;
    SET @VALUE = 0;
    SET @PriceType = 0;

    SET @vDate = CURDATE();

    SET @Result = 0;


    SELECT unit4_pricevolumes.value
    FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
        unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
    WHERE unit4_specialprices.customer_external_id = @vCustomerExternalId AND unit4_specialprices.sku = @vSku and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
    ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC LIMIT 1 INTO @Result;

    IF @Result > 0
    THEN
        SET @PriceFound = TRUE;
        SELECT unit4_pricevolumes.quantity, unit4_pricevolumes.value, CAST(unit4_specialprices.discount_type AS UNSIGNED) AS discount_type
        FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
            unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
        WHERE unit4_specialprices.customer_external_id = @vCustomerExternalId AND unit4_specialprices.sku = @vSku and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
        ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC;
    END IF;

    IF(!@PriceFound AND @vSku <> '' AND @vPriceListId <> '')
    THEN
        SELECT unit4_pricevolumes.value
        FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
            unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
        WHERE unit4_specialprices.pricelist_id = @vPriceListId AND unit4_specialprices.sku = @vSku and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
        ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC LIMIT 1 INTO @Result;


        IF @Result > 0
        THEN
            SET @PriceFound = TRUE;
            SELECT unit4_pricevolumes.quantity, unit4_pricevolumes.value, CAST(unit4_specialprices.discount_type AS UNSIGNED) AS discount_type
            FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
                unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
            WHERE unit4_specialprices.pricelist_id = @vPriceListId AND unit4_specialprices.sku = @vSku and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
            ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC;
        END IF;
    END IF;



    IF(!@PriceFound AND @vCustomerExternalId <> '' AND @vItemGroupId  <> '')
    THEN
        SELECT unit4_pricevolumes.value
        FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
            unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
        WHERE unit4_specialprices.customer_external_id = @vCustomerExternalId AND unit4_specialprices.itemgroup_id = @vItemGroupId and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
        ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC LIMIT 1 INTO @Result;

        IF @Result > 0
        THEN
            SET @PriceFound = TRUE;
            SELECT unit4_pricevolumes.quantity, unit4_pricevolumes.value, CAST(unit4_specialprices.discount_type AS UNSIGNED) AS discount_type
            FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
                unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
            WHERE unit4_specialprices.customer_external_id = @vCustomerExternalId AND unit4_specialprices.itemgroup_id = @vItemGroupId and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
            ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC;
        END IF;
    END IF;


    IF(!@PriceFound AND @vPriceListId <> '' AND @vItemGroupId <> '')
    THEN
        SELECT unit4_pricevolumes.value
        FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
            unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
        WHERE unit4_specialprices.pricelist_id = @vPriceListId AND unit4_specialprices.itemgroup_id = @vItemGroupId and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
        ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC LIMIT 1 INTO @Result;

        IF @Result > 0
        THEN
            SET @PriceFound = TRUE;
            SELECT unit4_pricevolumes.quantity, unit4_pricevolumes.value, CAST(unit4_specialprices.discount_type AS UNSIGNED) AS discount_type
            FROM unit4_pricevolumes INNER JOIN unit4_specialprices ON
                unit4_pricevolumes.unit4_specialprices_id = unit4_specialprices.id
            WHERE unit4_specialprices.pricelist_id = @vPriceListId AND unit4_specialprices.itemgroup_id = @vItemGroupId and (unit4_specialprices.date_from <= @vDate OR unit4_specialprices.date_from IS NULL) AND (unit4_specialprices.date_to >= @vDate OR unit4_specialprices.date_to IS NULL)
            ORDER BY unit4_pricevolumes.quantity DESC, unit4_specialprices.discount_type ASC;
        END IF;
    END IF;
END $$
DELIMITER ;
