<?php

namespace Conneqt\Unit4SpecialPrices\Model;

/**
 * @method \Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice getResource()
 * @method \Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\Collection getCollection()
 */
class SpecialPriceDataVolumePrice extends \Magento\Framework\Model\AbstractModel implements \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'unit4_specialprices_specialpricedatavolumeprice';
    protected $_cacheTag = 'unit4_specialprices_specialpricedatavolumeprice';
    protected $_eventPrefix = 'unit4_specialprices_specialpricedatavolumeprice';

    protected function _construct()
    {
        $this->_init(\Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed
     */
    public function getUnit4SpecialPriceId()
    {
        return $this->getData(self::FIELD_UNIT4_SPECIALPRICE_ID);
    }

    /**
     * @var $specialPriceId int
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setUnit4SpecialPriceId($specialPriceId)
    {
        $this->setData(self::FIELD_UNIT4_SPECIALPRICE_ID, $specialPriceId);

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->getData(self::FIELD_QUANTITY);
    }

    /**
     * @param $quantity int
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setQuantity($quantity)
    {
        $this->setData(self::FIELD_QUANTITY, $quantity);

        return $this;
    }

    /**
     * @return double
     */
    public function getValue()
    {
        return $this->getData(self::FIELD_VALUE);
    }

    /**
     * @param $value double
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setValue($value)
    {
        $this->setData(self::FIELD_VALUE, $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getDiscountType()
    {
        return $this->getData(self::FIELD_DISCOUNT_TYPE);
    }

    /**
     * @param $discountType string
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDiscountType($discountType)
    {
        $this->setData(self::FIELD_DISCOUNT_TYPE, $discountType);

        return $this;
    }
}
