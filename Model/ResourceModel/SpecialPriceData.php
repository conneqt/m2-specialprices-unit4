<?php

namespace Conneqt\Unit4SpecialPrices\Model\ResourceModel;

class SpecialPriceData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('unit4_specialprices', 'id');
    }
}
