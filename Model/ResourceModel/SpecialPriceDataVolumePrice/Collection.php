<?php

namespace Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Conneqt\Unit4SpecialPrices\Model\SpecialPriceDataVolumePrice::class, \Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    /**
     * @param $specialPriceId
     * @return \Conneqt\Unit4SpecialPrices\Model\SpecialPriceDataVolumePrice[]
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->addFieldToFilter('unit4_specialprices_id', ['eq' => $specialPriceId])->getItems();
    }
}
