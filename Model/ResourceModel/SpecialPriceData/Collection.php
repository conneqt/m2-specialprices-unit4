<?php

namespace Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Conneqt\Unit4SpecialPrices\Model\SpecialPriceData::class, \Conneqt\Unit4SpecialPrices\Model\ResourceModel\SpecialPriceData::class);
    }

    /**
     * @param $specialPriceId
     * @return \Conneqt\Unit4SpecialPrices\Model\SpecialPriceData
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->getItemByColumnValue(\Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataInterface::FIELD_SPECIALPRICE_ID, $specialPriceId);
    }
}
