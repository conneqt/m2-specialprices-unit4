<?php

namespace Conneqt\Unit4SpecialPrices\Api\Data;

interface SpecialPriceDataVolumePriceInterface
{
    const FIELD_ID = 'id';
    const FIELD_UNIT4_SPECIALPRICE_ID = 'unit4_specialprices_id';
    const FIELD_DISCOUNT_TYPE = 'discount_type';
    const FIELD_QUANTITY = 'quantity';
    const FIELD_VALUE = 'value';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getUnit4SpecialPriceId();

    /**
     * @var $specialPriceId int
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setUnit4SpecialPriceId($specialPriceId);

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param $quantity int
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setQuantity($quantity);

    /**
     * @return double
     */
    public function getValue();

    /**
     * @param $value double
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setValue($value);

    /**
     * @return string
     */
    public function getDiscountType();

    /**
     * @param $discountType string
     * @return \Conneqt\Unit4SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDiscountType($discountType);
}
